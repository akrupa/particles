// OpenGL Graphics includes
#include <GL/glew.h>
#if defined (_WIN32)
#include <GL/wglew.h>
#endif
#if defined(__APPLE__) || defined(__MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/freeglut.h>
#endif

// CUDA runtime
#include <cuda_runtime.h>

// CUDA utilities and system includes
#include <helper_functions.h>
#include <helper_cuda.h>    // includes cuda.h and cuda_runtime_api.h
#include <helper_cuda_gl.h> // includes cuda_gl_interop.h// includes cuda_gl_interop.h

// Includes
#include <stdlib.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <unistd.h>

#include "particleSet.h"

#include "paramgl.h"

#define MAX_EPSILON_ERROR 5.00f
#define THRESHOLD         0.30f

#define NUM_PARTICLES 100
#define GRID_SIZE 64

const uint width = 800, height = 800;

uint numberOfParticles;
bool displayEnabled = true;
bool displaySliders = false;
bool useGPU = false;
uint2 gridSize;

// simulation parameters
float timestep = 0.5f;
float damping = 1.0f;
float gravity = 0.0003f;
int iterations = 1;
int ballr = 10;

float collideSpring = 0.5f;;
float collideDamping = 0.02f;;
float collideShear = 0.1f;
float collideAttraction = 0.0f;

particleSet *particleSet = 0;

// fps
static int fpsCount = 0;
static int fpsLimit = 1;
StopWatchInterface *timer = NULL;

ParamListGL *params;

extern "C" void cudaInit(int argc, char **argv);
extern "C" void cudaGLInit(int argc, char **argv);

void initParticleSet(int numberOfParticles, uint3 gridSize)
{
	//particleSet = new particleSet(numberOfParticles, gridSize);
	//particleSet->reset(ParticleSystem::CONFIG_GRID);

	//renderer = new ParticleRenderer;
	//renderer->setParticleRadius(psystem->getParticleRadius());
	//renderer->setColorBuffer(psystem->getColorBuffer());

    sdkCreateTimer(&timer);
}

void cleanup()
{
    sdkDeleteTimer(&timer);
}

// initialize OpenGL
void initGL(int *argc, char **argv)
{
    glutInit(argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutCreateWindow("CUDA Particles");

    glewInit();

    if (!glewIsSupported("GL_VERSION_2_0 GL_VERSION_1_5 GL_ARB_multitexture GL_ARB_vertex_buffer_object"))
    {
        fprintf(stderr, "Required OpenGL extensions missing.");
        exit(EXIT_FAILURE);
    }

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.25, 0.25, 0.25, 1.0);

    glutReportErrors();
}

void computeFPS()
{
    fpsCount++;
    printf("%d %d", fpsCount, fpsLimit);
    if (fpsCount == fpsLimit)
    {
        char fps[256];
        float ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
        if(useGPU)printf("on\n");
        else printf("off");
        if(useGPU)
        	sprintf(fps, "CUDA Particles (%d particles): %3.1f fps, GPU=on", numberOfParticles, ifps);
        else
        	sprintf(fps, "CUDA Particles (%d particles): %3.1f fps, GPU=off", numberOfParticles, ifps);
        glutSetWindowTitle(fps);
        fpsCount = 0;

        fpsLimit = (int)MAX(ifps, 1.f);
        if(fpsLimit < 1) fpsLimit = 1;
        sdkResetTimer(&timer);
    }
}

void display()
{
	printf("displaye\n");
    sdkStartTimer(&timer);

    //particleSet->setDamping(damping);
    //particleSet->setGravity(-gravity);
    //particleSet->setCollideSpring(collideSpring);
    //particleSet->setCollideDamping(collideDamping);
    //particleSet->setCollideShear(collideShear);
    //particleSet->setCollideAttraction(collideAttraction);
    //particleSet->useGpu(useGPU);

    //particleSet->update(timestep);

    // render
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    // cube
    glColor3f(1.0, 1.0, 0.0);
    glutWireCube(1.8);

    //particleSet->display(displayMode);

    if (displaySliders)
    {
        glDisable(GL_DEPTH_TEST);
        glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ZERO); // invert color
        glEnable(GL_BLEND);
        params->Render(0, 0);
        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
    }
    sdkStopTimer(&timer);

    glutSwapBuffers();
    glutReportErrors();

    computeFPS();
}

void reshape(int width, int height)
{
	int size = width>height ? height : width;
    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, size, size);
    printf("%d x %d\n", width, height);
    //if (renderer)
    //{
        //renderer->setWindowSize(w, h);
        //renderer->setFOV(60.0);
    //}
}

void key(unsigned char key, int /*x*/, int /*y*/)
{
    switch (key)
    {
        case ' ':
            useGPU = !useGPU;
            break;

        case 13:
            /*
            psystem->update(timestep);

            if (renderer)
            {
                renderer->setVertexBuffer(psystem->getCurrentReadBuffer(), psystem->getNumParticles());
            }
			*/
            break;

        case '\033':
        case 'q':
            exit(EXIT_SUCCESS);
            break;

        case 'p':
            //displayMode = (ParticleRenderer::DisplayMode)
            //              ((displayMode + 1) % ParticleRenderer::PARTICLE_NUM_MODES);
            break;

        case 'd':
            //psystem->dumpGrid();
            break;

        case 'u':
            //psystem->dumpParticles(0, numParticles-1);
            break;

        case 'r':
            //displayEnabled = !displayEnabled;
            break;

        case '1':
            //psystem->reset(ParticleSystem::CONFIG_GRID);
            break;

        case '2':
            //psystem->reset(ParticleSystem::CONFIG_RANDOM);
            break;

        case '3':
            //addSphere();
            break;

        case 'h':
            displaySliders = !displaySliders;
            break;
    }

    glutPostRedisplay();
}

void initParams()
{
	params = new ParamListGL("misc");
	params->AddParam(new Param<float>("time step", timestep, 0.0f, 1.0f, 0.01f, &timestep));
	params->AddParam(new Param<float>("damping"  , damping , 0.0f, 1.0f, 0.001f, &damping));
	params->AddParam(new Param<float>("gravity"  , gravity , 0.0f, 0.001f, 0.0001f, &gravity));
	params->AddParam(new Param<int> ("ball radius", ballr , 1, 20, 1, &ballr));

	params->AddParam(new Param<float>("collide spring" , collideSpring , 0.0f, 1.0f, 0.001f, &collideSpring));
	params->AddParam(new Param<float>("collide damping", collideDamping, 0.0f, 0.1f, 0.001f, &collideDamping));
	params->AddParam(new Param<float>("collide shear"  , collideShear  , 0.0f, 0.1f, 0.001f, &collideShear));
	params->AddParam(new Param<float>("collide attract", collideAttraction, 0.0f, 0.1f, 0.001f, &collideAttraction));
}

void mainMenu(int i)
{
    key((unsigned char) i, 0, 0);
}

void initMenus()
{
    glutCreateMenu(mainMenu);
    glutAddMenuEntry("Reset block [1]", '1');
    glutAddMenuEntry("Reset random [2]", '2');
    glutAddMenuEntry("Add sphere [3]", '3');
    glutAddMenuEntry("View mode [v]", 'v');
    glutAddMenuEntry("Move cursor mode [m]", 'm');
    glutAddMenuEntry("Toggle point rendering [p]", 'p');
    glutAddMenuEntry("Toggle animation [ ]", ' ');
    glutAddMenuEntry("Step animation [ret]", 13);
    glutAddMenuEntry("Toggle sliders [h]", 'h');
    glutAddMenuEntry("Quit (esc)", '\033');
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
    printf("CUDA particles starting...\n\n");

    numberOfParticles = NUM_PARTICLES;
    uint gridDim = GRID_SIZE;

    gridSize.x = gridSize.y = gridDim;
    printf("grid: %d x %d = %d cells\n", gridSize.x, gridSize.y, gridSize.x*gridSize.y);
    printf("particles: %d\n", numberOfParticles);

    initGL(&argc, argv);
    cudaGLInit(argc, argv);

    //initParticleSystem(numParticles, gridSize, g_refFile==NULL);
    initParams();
    initMenus();


    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    //glutMouseFunc(mouse);
    //glutMotionFunc(motion);
    glutKeyboardFunc(key);
    //glutSpecialFunc(special);
    //glutIdleFunc(idle);

    atexit(cleanup);


    glutMainLoop();

    //if (psystem)
    //{
    //    delete psystem;
    //}

    cudaDeviceReset();
    return EXIT_SUCCESS;
}
